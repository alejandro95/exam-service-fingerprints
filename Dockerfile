FROM maven:3.6-jdk-8-alpine AS builder
VOLUME /tmp
ADD ./target/exam-service-fingerprints-0.0.1-SNAPSHOT.jar service-fingerprints.jar
ENTRYPOINT ["java","-jar","/service-fingerprints.jar"]