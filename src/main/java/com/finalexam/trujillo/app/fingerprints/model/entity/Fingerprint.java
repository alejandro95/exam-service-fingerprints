package com.finalexam.trujillo.app.fingerprints.model.entity;

import lombok.*;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Fingerprint {

    private String entityName;
    private Boolean success;

}
