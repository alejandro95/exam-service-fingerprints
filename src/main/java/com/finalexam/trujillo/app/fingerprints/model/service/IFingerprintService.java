package com.finalexam.trujillo.app.fingerprints.model.service;

import com.finalexam.trujillo.app.fingerprints.model.entity.Fingerprint;
import com.finalexam.trujillo.app.fingerprints.model.response.FingerprintResponse;
import io.reactivex.Single;

public interface IFingerprintService {

    Single<FingerprintResponse> validateFingerprint(String document);

}
