package com.finalexam.trujillo.app.fingerprints.model.service;

import com.finalexam.trujillo.app.fingerprints.model.response.FingerprintResponse;
import io.reactivex.Single;
import org.springframework.stereotype.Service;

@Service
public class FingerprintServiceImpl implements IFingerprintService {


    @Override
    public Single<FingerprintResponse> validateFingerprint(String document) {

        return Single.just(FingerprintResponse.builder()
                .entityName("Core")
                .success(true)
                .build());

    }
}
