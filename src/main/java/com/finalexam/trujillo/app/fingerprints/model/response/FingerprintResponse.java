package com.finalexam.trujillo.app.fingerprints.model.response;

import lombok.*;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class FingerprintResponse {

    private String entityName;
    private Boolean success;

}
