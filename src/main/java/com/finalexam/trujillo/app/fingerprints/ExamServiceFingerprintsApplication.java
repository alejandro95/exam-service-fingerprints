package com.finalexam.trujillo.app.fingerprints;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ExamServiceFingerprintsApplication {

    public static void main(String[] args) {
        SpringApplication.run(ExamServiceFingerprintsApplication.class, args);
    }

}
