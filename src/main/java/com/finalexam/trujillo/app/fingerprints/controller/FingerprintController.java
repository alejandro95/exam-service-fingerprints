package com.finalexam.trujillo.app.fingerprints.controller;


import com.finalexam.trujillo.app.fingerprints.model.request.FingerPrintRequest;
import com.finalexam.trujillo.app.fingerprints.model.response.FingerprintResponse;
import com.finalexam.trujillo.app.fingerprints.model.service.IFingerprintService;
import io.reactivex.Single;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/core/fingerprints")
public class FingerprintController {

    @Autowired
    private IFingerprintService iFingerprintService;

    @PostMapping("/validate")
    public Single<FingerprintResponse> validateFingerprint(@RequestBody FingerPrintRequest fingerPrintRequest) {

        return iFingerprintService.validateFingerprint(fingerPrintRequest.getDocument());
    }

}
